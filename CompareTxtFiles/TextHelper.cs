﻿using System;


namespace CompareTxtFiles
{
    static class TextHelper
    {
        // length - items to take FROM index
        // int[] data = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
        // int[] sub = data.SubArray(3, 4); // contains {3,4,5,6}
        public static T[] SubArray<T>(this T[] data, int index, int length)
        {
            
            T[] result = new T[length];
            Array.Copy(data, index, result, 0, length);
            return result;
        }
        public static string[] splitText(string Text)
        {
            string[] Lines;
            Text = Text.Replace("\r", "");
            Lines = Text.Split('\n');
            return Lines;
        }
        public static string[] subText(string[] Lines, int start, int end)
        {
            //int x = Lines.Length - start - end;
            if (end == 0)
            {
                Lines = Lines.SubArray(start, Lines.Length - start); // default Lines.Length - start, TO take all lines

            }
            else
            {
                Lines = Lines.SubArray(start, end - (start - 1));
            }
            return Lines;
        }
    }
}
