namespace CompareTxtFiles
{
    using System;
    using System.Collections;
    using System.Text;
    using System.Text.RegularExpressions;

    public class TextComparer
    {

        public struct TextInfo
        {
            public int StartA; // ������ ������ � ������ �
            public int StartB; // ������ ������ � ������ B
            public string contentA;
            public string contentB;

            public int deletedA; // ����� ��������� � �
            public int insertedB; // ����� ������� � �
        }
    

        private struct SMSRD
        {
            internal int x, y;
        }





        public static TextInfo? CompText(string[] LinesA, string[] LinesB, int start, int length)
        {
            return (CompText(LinesA, LinesB, start, length, false, false, false));
        }

        // ������� �������� � 2 �����. ����������, ��������� ������
        // �������� ���������� 2 ������� �����: ������ ������ 2 ������ �������������� � ��� (�����). 
        // ���� ��� ����������� ��������� �������� ���� ����� � ������� ���-�������, 
        // ��� ��� � ��� ����� ����� ��������� � �������� ����� ����� ������ ���, ����� ��������� ����� ������




        // ���������� ������ TextInfo, ������� ��������� �������� 
        public static TextInfo? CompText(string[] LinesA, string[] LinesB, int start, int length, bool trimSpace, bool ignoreSpace, bool ignoreCase)
        // trimSpace - ��� true ��� ������� � ������ � ����� ��������� ����� ���������� ���������
        // ignoreSpace - �� ��. ������� (������������� � ����)
        // ignoreCase - �� ��. ������� (�� ������. � ������ �������)
        {
            Hashtable h;
            try
            {
                h = new Hashtable(length);

            }
            catch (OutOfMemoryException e)
            {
                Console.WriteLine("������� ������� ������� ������������ ������.");
                return null;
            }
            
            CompData DataA = new CompData(CompCodes(h, LinesA, trimSpace, ignoreSpace, ignoreCase), LinesA);
            CompData DataB = new CompData(CompCodes(h, LinesB, trimSpace, ignoreSpace, ignoreCase), LinesB);

            h = null;

            int MAX = DataA.Length + DataB.Length + 1;
            int[] DownVector = new int[2 * MAX + 2];
            int[] UpVector = new int[2 * MAX + 2];

            LCS(DataA, 0, DataA.Length, DataB, 0, DataB.Length, DownVector, UpVector);

            Optimize(DataA);
            Optimize(DataB);
            return CreateCompResults(DataA, DataB, start);
        } // DiffText




        private static void Optimize(CompData Data)
        {
            int StartPos, EndPos;

            StartPos = 0;
            while (StartPos < Data.Length)
            {
                while ((StartPos < Data.Length) && (Data.modified[StartPos] == false))
                    StartPos++;
                EndPos = StartPos;
                while ((EndPos < Data.Length) && (Data.modified[EndPos] == true))
                    EndPos++;

                if ((EndPos < Data.Length) && (Data.data[StartPos] == Data.data[EndPos]))
                {
                    Data.modified[StartPos] = false;
                    Data.modified[EndPos] = true;
                }
                else
                {
                    StartPos = EndPos;
                } // if
            } // while
        } // Optimize






        private static int[] CompCodes(Hashtable h, string[] Lines, bool trimSpace, bool ignoreSpace, bool ignoreCase)
        {
            
            int[] Codes;
            int lastUsedCode = h.Count;
            object aCode;
            string s;

            //aText = aText.Replace("\r", "");
            //Lines = aText.Split('\n');

            Codes = new int[Lines.Length];

            for (int i = 0; i < Lines.Length; ++i)
            {
                s = Lines[i];
                if (trimSpace)
                    s = s.Trim();

                if (ignoreSpace)
                {
                    s = Regex.Replace(s, "\\s+", " ");
                }

                if (ignoreCase)
                    s = s.ToLower();

                aCode = h[s];
                if (aCode == null)
                {
                    lastUsedCode++;
                    h[s] = lastUsedCode;
                    Codes[i] = lastUsedCode;
                }
                else
                {
                    Codes[i] = (int)aCode;
                } // if
            } // for
            return (Codes);
        } // DiffCodes


        private static SMSRD SMS(CompData DataA, int LowerA, int UpperA, CompData DataB, int LowerB, int UpperB,
          int[] DownVector, int[] UpVector)
        {

            SMSRD ret;
            int MAX = DataA.Length + DataB.Length + 1;

            int DownK = LowerA - LowerB;
            int UpK = UpperA - UpperB;

            int Delta = (UpperA - LowerA) - (UpperB - LowerB);
            bool oddDelta = (Delta & 1) != 0;

            int DownOffset = MAX - DownK;
            int UpOffset = MAX - UpK;

            int MaxD = ((UpperA - LowerA + UpperB - LowerB) / 2) + 1;


            DownVector[DownOffset + DownK + 1] = LowerA;
            UpVector[UpOffset + UpK - 1] = UpperA;

            for (int D = 0; D <= MaxD; D++)
            {

                for (int k = DownK - D; k <= DownK + D; k += 2)
                {

                    int x, y;
                    if (k == DownK - D)
                    {
                        x = DownVector[DownOffset + k + 1]; // down
                    }
                    else
                    {
                        x = DownVector[DownOffset + k - 1] + 1; // a step to the right
                        if ((k < DownK + D) && (DownVector[DownOffset + k + 1] >= x))
                            x = DownVector[DownOffset + k + 1]; // down
                    }
                    y = x - k;

                    while ((x < UpperA) && (y < UpperB) && (DataA.data[x] == DataB.data[y]))
                    {
                        x++; y++;
                    }
                    DownVector[DownOffset + k] = x;

                    if (oddDelta && (UpK - D < k) && (k < UpK + D))
                    {
                        if (UpVector[UpOffset + k] <= DownVector[DownOffset + k])
                        {
                            ret.x = DownVector[DownOffset + k];
                            ret.y = DownVector[DownOffset + k] - k;
                            return (ret);
                        } // if
                    } // if

                } // for k

                for (int k = UpK - D; k <= UpK + D; k += 2)
                {

                    int x, y;
                    if (k == UpK + D)
                    {
                        x = UpVector[UpOffset + k - 1];
                    }
                    else
                    {
                        x = UpVector[UpOffset + k + 1] - 1;
                        if ((k > UpK - D) && (UpVector[UpOffset + k - 1] < x))
                            x = UpVector[UpOffset + k - 1];
                    } // if
                    y = x - k;

                    while ((x > LowerA) && (y > LowerB) && (DataA.data[x - 1] == DataB.data[y - 1]))
                    {
                        x--; y--;
                    }
                    UpVector[UpOffset + k] = x;

                    if (!oddDelta && (DownK - D <= k) && (k <= DownK + D))
                    {
                        if (UpVector[UpOffset + k] <= DownVector[DownOffset + k])
                        {
                            ret.x = DownVector[DownOffset + k];
                            ret.y = DownVector[DownOffset + k] - k;
                            return (ret);
                        } // if
                    } // if

                } // for k

            } // for D

            throw new ApplicationException("�������� �� ������ ��������� �� ������� ����� ����.");
        } // SMS



        private static void LCS(CompData DataA, int LowerA, int UpperA, CompData DataB, int LowerB, int UpperB, int[] DownVector, int[] UpVector)
        {

            while (LowerA < UpperA && LowerB < UpperB && DataA.data[LowerA] == DataB.data[LowerB])
            {
                LowerA++; LowerB++;
            }

            while (LowerA < UpperA && LowerB < UpperB && DataA.data[UpperA - 1] == DataB.data[UpperB - 1])
            {
                --UpperA; --UpperB;
            }

            if (LowerA == UpperA)
            {
                // �������� ��� �����������
                while (LowerB < UpperB)
                    DataB.modified[LowerB++] = true;

            }
            else if (LowerB == UpperB)
            {
                // �������� ��� ���������
                while (LowerA < UpperA)
                    DataA.modified[LowerA++] = true;

            }
            else
            {
                SMSRD smsrd = SMS(DataA, LowerA, UpperA, DataB, LowerB, UpperB, DownVector, UpVector);

                LCS(DataA, LowerA, smsrd.x, DataB, LowerB, smsrd.y, DownVector, UpVector);
                LCS(DataA, smsrd.x, UpperA, DataB, smsrd.y, UpperB, DownVector, UpVector);  // 2002.09.20: no need for 2 points 
            }
        } // LCS()



        private static TextInfo? CreateCompResults(CompData DataA, CompData DataB, int shift)
        {
            TextInfo aItem;

            int StartA, StartB;
            int LineA, LineB;

            LineA = 0;
            LineB = 0;
            while (LineA < DataA.Length || LineB < DataB.Length)
            {
                if ((LineA < DataA.Length) && (!DataA.modified[LineA])
                  && (LineB < DataB.Length) && (!DataB.modified[LineB]))
                {
                    LineA++;
                    LineB++;

                }
                else
                {
                    StartA = LineA;
                    StartB = LineB;

                    while (LineA < DataA.Length && (LineB >= DataB.Length || DataA.modified[LineA]))
                        LineA++;

                    while (LineB < DataB.Length && (LineA >= DataA.Length || DataB.modified[LineB]))
                        LineB++;

                    if ((StartA < LineA) || (StartB < LineB))
                    {
                        aItem = new TextInfo();
                        aItem.StartA = StartA + shift;
                        aItem.StartB = StartB + shift;
                        aItem.deletedA = LineA - StartA;
                        aItem.insertedB = LineB - StartB;
                        aItem.contentA = DataA.contents[StartA];
                        aItem.contentB = DataB.contents[StartB];
                        return (aItem);

                    } // if
                } // if
            } // while
            return null;


        }

        
        public static string TestHelper(TextInfo[] f)
        {
            StringBuilder ret = new StringBuilder();
            for (int n = 0; n < f.Length; n++)
            {
                ret.Append(f[n].deletedA.ToString() + "." + f[n].insertedB.ToString() + "." + f[n].StartA.ToString() + "." + f[n].StartB.ToString() + "*");
            }
            // Debug.Write(5, "TestHelper", ret.ToString());
            return (ret.ToString());
        }

       
    }


    internal class CompData
    {

        internal int Length;

        internal int[] data;

        internal bool[] modified;

        internal string[] contents;


        internal CompData(int[] initData, string[] initContents)
        {
            data = initData;
            Length = initData.Length;
            modified = new bool[Length + 2];
            contents = initContents;
        }


    }


}