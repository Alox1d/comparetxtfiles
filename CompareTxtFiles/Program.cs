﻿using System;
using static CompareTxtFiles.TextComparer;

namespace CompareTxtFiles
{
    class Program
    {
        private static readonly int WRONG_VALUE = -1;
        private static readonly int START_DEFAULT_VALUE = 0;


        static void Main(string[] args)
        {

            //string a = "HELLO\nWORLD";
            //string b = "\n\nhello\n\n\n\nworld\n";
            //TextInfo[] infos = TextComparer.CompText("ыы ти", "ыы ти вык", false, false, false);
            ////int numberOfDiff = 0;
            //Console.WriteLine("Строки с расхождением в эталонном файле: ");

            //foreach (TextInfo textInfo in infos)
            //{
            //    //numberOfDiff++;
            //    Console.WriteLine(textInfo.StartA);
            //}
            //Console.WriteLine("Строки с расхождением в результирующем файле: ");
            //foreach (TextInfo textInfo in infos)
            //{
            //    Console.WriteLine(textInfo.StartB);
            //}
            //Console.ReadLine();


            Console.Write("Доброго времени суток!\nВас приветствует программа для сравнения двух текстовых файлов. " +
                "\nПожалуйста, введите путь к эталонному файлу:");
            // чтение из файла
            FileHelper fileHelper = FileHelper.getInstance();


            //string firstFilePath = @"G:\nnnnnn.txt";
            String firstFilePath = Console.ReadLine();
            string textFromFirstFile = fileHelper.openFile(firstFilePath);
            if (textFromFirstFile == null)
            {
                closeProgram();
                return;
            }
            Console.WriteLine(" " + firstFilePath);
            Console.Write("Отлично! Теперь введите путь к результирующему файлу:");
            //string secondFilePath = @"G:\nnnnnn.txt";
            String secondFilePath = Console.ReadLine();
            string textFromSecondFile = fileHelper.openFile(secondFilePath);
            if (textFromSecondFile == null)
            {
                closeProgram();
                return;
            }
            Console.WriteLine(" " + secondFilePath);

            // split
            string[] LinesA = TextHelper.splitText(textFromFirstFile);
            string[] LinesB = TextHelper.splitText(textFromSecondFile);

            // от какой строки
            Console.WriteLine("Введите строку, с которой начинать сравнение " +
                "(Enter для значения по умолч.): ");
            int startFrom = parseNumber(Console.ReadLine());

            if (startFrom == WRONG_VALUE)
            {
                Console.WriteLine("Не удалось преобразовать значение к числу!");
                closeProgram();
                return;
            }
            else
            if (startFrom == START_DEFAULT_VALUE)
            {
                Console.WriteLine("Выбрано значение по умолчанию.");
            }
            else
            {
                startFrom--;
                if (startFrom < 0 || startFrom > LinesA.Length - 1 || startFrom > LinesB.Length - 1)
                {
                    Console.WriteLine("Такой строки нет в одном из файлов.");
                    closeProgram();
                    return;
                }
            }


            // до какой строки
            Console.WriteLine("Введите строку, на которой заканчивать сравнение " +
                "(Enter для значения по умолч.): ");
            int endTo = parseNumber(Console.ReadLine());

            if (endTo == WRONG_VALUE)
            {
                Console.WriteLine("Не удалось преобразовать значение к числу!");
                closeProgram();
                return;
            }
            else
            if (endTo == START_DEFAULT_VALUE)
            {

                Console.WriteLine("Выбрано значение по умолчанию.");
            }
            else
            {
                endTo--;
                if (endTo < 0 || endTo > LinesA.Length - 1 || endTo > LinesB.Length - 1)
                {
                    Console.WriteLine("Такой строки нет в одном из файлов.");
                    closeProgram();
                    return;
                }
            }



            Console.WriteLine("Ожидайте, работаем...");

            //string a = "HELLO\nWORLD";
            //string b = "\n\nhello\n\n\n\nworld\n";

            LinesA = TextHelper.subText(LinesA, startFrom, endTo);
            LinesB = TextHelper.subText(LinesB, startFrom, endTo);
            TextInfo? info = TextComparer.CompText(LinesA, LinesB, startFrom, textFromFirstFile.Length + textFromSecondFile.Length);

            if (info == null)
            {
                Console.WriteLine("Различий не найдено. Файлы безразличны друг другу с данной строки.");

            }
            else
            {
                Console.WriteLine("Великолепно! Результат сравнения:\n");
                Console.WriteLine("Строки с расхождением в эталонном файле: ");

                Console.Write(info.Value.StartA + 1 + " ");
                Console.WriteLine(info.Value.contentA);
                Console.WriteLine("Строки с расхождением в результирующем файле: ");

                Console.Write(info.Value.StartB + 1 + " ");
                Console.WriteLine(info.Value.contentB);

                Console.ReadLine();

            }
            Console.ReadLine();





        }

        private static void closeProgram()
        {
            Console.WriteLine("Программа завершена.");
            Console.ReadLine();
            return;
        }
        private static int parseNumber(string input)
        {
            int res;
            if (Int32.TryParse(input, out res))
            {

                return res;
            }
            if (string.IsNullOrEmpty(input))
            {
                return START_DEFAULT_VALUE;
            }
            return WRONG_VALUE;
        }
    }
}
