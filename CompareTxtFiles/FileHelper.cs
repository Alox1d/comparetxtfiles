﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace CompareTxtFiles
{
    class FileHelper
    {
        private static FileHelper instance;

        private FileHelper()
        { }

        public static FileHelper getInstance()
        {
            if (instance == null)
                instance = new FileHelper();
            return instance;
        }

        internal string openFile(string path)
        {
            try
            {


                using (FileStream fstream = File.OpenRead($"{path}"))
                {
                    // преобразуем строку в байты
                    byte[] array = new byte[fstream.Length];
                    // считываем данные
                    fstream.Read(array, 0, array.Length);
                    // декодируем байты в строку
                    string textFromFile = System.Text.Encoding.Default.GetString(array);
                    return textFromFile;
                }
            }
            catch (FileNotFoundException e)
            {
                Console.WriteLine("Неверный путь к файлу!");
                return null;

            }

        }
    }
}
